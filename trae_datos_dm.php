<?php
require_once('conexion.php');
$id_dm = $_POST['id'];
$sql1 = "SELECT dm_codigo,dm_id,dm_estado,mt_motivo,Nombre,dm_fcreacion,dm_observacion,dm_id_mt,dm_id_doc,dm_id_dir,
    (SELECT mo_nombre FROM prg.des_destinos 
    JOIN prg.mo_motoristas on des_id_mo=mo_id WHERE des_id_dm = dm_id) motorista,
    (SELECT des_fecha_asignacion FROM prg.des_destinos 
    JOIN prg.mo_motoristas on des_id_mo=mo_id WHERE des_id_dm = dm_id) fasignacion,
    (SELECT des_hora_asignacion FROM prg.des_destinos 
    JOIN prg.mo_motoristas on des_id_mo=mo_id WHERE des_id_dm = dm_id) hasignacion,
    (SELECT Nombre FROM prg.divisiones WHERE pludivision=dm_id_suc_destino) sucursal,
    (SELECT cli_email FROM prg.cli_clientes WHERE cli_id=dm_id_cli) correo,
    (SELECT cli_nombre FROM prg.cli_clientes WHERE cli_id=dm_id_cli) nombre_cliente
    FROM prg.dm_domicilios 
    JOIN prg.mt_motivos ON dm_id_mt=mt_id 
    JOIN prg.usu_usuarios ON dm_id_usu=usu_id
    JOIN prg.gestores on id_codigo_gestor=plugestor   
    WHERE dm_id= '" . $id_dm . "' ";
$ds = odbc_exec($conn, $sql1);
while ($fila = odbc_fetch_array($ds)) {
    $codigo = $fila['dm_codigo'];
    $id = $fila['dm_id'];
    $estado = $fila['dm_estado'];
    $motivo = $fila['mt_motivo'];
    $vendedor = utf8_encode($fila['Nombre']);
    $fecha = date('d-m-Y', strtotime($fila['dm_fcreacion']));
    $hora = date('H:i:s', strtotime($fila['dm_fcreacion']));
    $observacion = $fila['dm_observacion'];
    $motorista = ($fila['motorista'] == "") ? "Pendiente de Asignar" : $fila['motorista'];
    $fasignacion = ($fila['fasignacion'] == "") ? "Pendiente de Asignar" : date('d-m-Y', strtotime($fila['fasignacion']));
    $hasignacion = ($fila['hasignacion'] == "") ? "Pendiente de Asignar" : $fila['hasignacion'];
    $tipo = $fila['dm_id_mt'];
    $dm_id_doc = $fila['dm_id_doc'];
    $sucursal = $fila['sucursal'];
    $correo = $fila['correo'];
    $cliente = $fila['nombre_cliente'];
    $id_dir = $fila['dm_id_dir'];
    
    if ($tipo == 2 || $tipo == 4) {
        /**
         * para ventas
         */
        $numfact1 = '';
        $numberFat = "SELECT Codigo FROM prg.docclientesm where PLUDocCliente=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact1 = odbc_fetch_object($fact);

        echo json_encode(array(
            "codigo" => $codigo, "id" => $id, "estado" => $estado,
            "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista),
            "fasignacion" => $fasignacion, "hasignacion" => $hasignacion, "tipo" => $tipo,
            "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, "codigoFact" => $numfact1->Codigo, 'id_dir' => $id_dir
        ));
    }
    if ($tipo == 11) {
        /**
         * para traslados
         */
        $numfact1 = '';
        $numberFat = "SELECT Codigo FROM  prg.trasladosm where PLUTraslado=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact1 = odbc_fetch_object($fact);

        echo json_encode(array(
            "codigo" => $codigo, "id" => $id, "estado" => $estado,
            "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista),
            "fasignacion" => $fasignacion, "hasignacion" => $hasignacion, "tipo" => $tipo,
            "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, "codigoFact" => $numfact1->Codigo, 'id_dir' => $id_dir
        ));
    }
    // recoger muestra
    if ($tipo == 6) {

        echo json_encode(array(
            "codigo" => $codigo, "id" => $id, "estado" => $estado, "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista), "fasignacion" => $fasignacion, "hasignacion" => $hasignacion,
            "tipo" => $tipo, "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, 'id_dir' => $id_dir
        ));
    }
    // compra en plaza
    if ($tipo == 7) {
        $numfact1 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact1 = odbc_fetch_object($fact);
        //echo $motorista)
        echo json_encode([
            "codigo" => $codigo, "id" => $id, "estado" => $estado, "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista), "fasignacion" => $fasignacion, "hasignacion" => $hasignacion,
            "tipo" =>  $tipo, "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, "n_orden" => $numfact1->n_orden, 'id_dir' => $id_dir
        ]);
    }
    // taller
    if ($tipo == 14) {
        $numfact1 = '';
        $numberFat = "SELECT n_orden FROM  prg.dm_otros where id=$dm_id_doc";
        $fact = odbc_exec($conn, $numberFat);

        $numfact1 = odbc_fetch_object($fact);

        echo json_encode(array(
            "codigo" => $codigo, "id" => $id, "estado" => $estado, "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista), "fasignacion" => $fasignacion, "hasignacion" => $hasignacion,
            "tipo" =>  $tipo, "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, "n_orden" => $numfact1->n_orden, 'id_dir' => $id_dir
        ));
    }
    // cobros
    if ($tipo == 1) {
        echo json_encode(array(
            "codigo" => $codigo, "id" => $id, "estado" => $estado, "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista), "fasignacion" => $fasignacion, "hasignacion" => $hasignacion,
            "tipo" => $tipo, "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, 'id_dir' => $id_dir
        ));
    }
    // visita a cliente programada
    if ($tipo == 12) {
        echo json_encode(array(
            "codigo" => $codigo, "id" => $id, "estado" => $estado, "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista), "fasignacion" => $fasignacion, "hasignacion" => $hasignacion,
            "tipo" => $tipo, "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, 'id_dir' => $id_dir
        ));
    }
    // administrativo
    if ($tipo == 9) {
        echo json_encode(array(
            "codigo" => $codigo, "id" => $id, "estado" => $estado, "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista), "fasignacion" => $fasignacion, "hasignacion" => $hasignacion,
            "tipo" => $tipo, "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, 'id_dir' => $id_dir
        ));
    }
    // retorno
    if ($tipo == 15) {
        echo json_encode(array(
            "codigo" => $codigo, "id" => $id, "estado" => $estado, "motivo" => $motivo, "vendedor" => $vendedor, "fecha" => $fecha,
            "hora" => $hora, "observacion" => $observacion, "motorista" => utf8_encode($motorista), "fasignacion" => $fasignacion, "hasignacion" => $hasignacion,
            "tipo" => $tipo, "doc" => $dm_id_doc, "sucursal" => $sucursal, "correo" => $correo, "cliente" => $cliente, 'id_dir' => $id_dir
        ));
    }
}
